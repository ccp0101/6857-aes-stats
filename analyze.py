import sys
from scipy import stats

def ks_test(rvs1, rvs2):
    return stats.ks_2samp(rvs1, rvs2)

def student_t_test(rvs1, rvs2):
    return stats.ttest_ind(rvs1, rvs2, equal_var=False)


samples = {}
for i in xrange(1, 11):
    samples[i] = []

for line in sys.stdin:
    nrounds, datum = map(int, line.strip().split())
    samples[nrounds].append(datum)

r10 = samples[10]

for i in xrange(1, 10):
    ks_result = ks_test(samples[i], r10)
    print "KS-test:", i, ks_result

    t_result = student_t_test(samples[i], r10)
    print "T-test:", i, t_result
