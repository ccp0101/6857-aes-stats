#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include "aes256.h"

//  gcc -std=c99 aes256.c f.c -o f

static const char* RAND_FILE = "/dev/urandom";

int main (int argc, char *argv[])
{
    aes256_context ctx;
    uint8_t K[32];
    uint8_t M[16];
    uint8_t buf[16];
    int qbyte[256];

    assert(argc == 5);

    int maxr = atoi(argv[1]);
    int p = atoi(argv[2]);
    int q = atoi(argv[3]);
    int n = atoi(argv[4]);
    int ret = 0;

    int rand_fd = open(RAND_FILE, O_RDONLY);

    for (int step = 0; step < n; step++) {
        ret = read(rand_fd, K, sizeof(K));
        assert(ret == sizeof(K));
        ret = read(rand_fd, M, sizeof(M));
        assert(ret == sizeof(M));

        for (int r = 1; r <= maxr; r++) {
            for (int i = 0; i < 256; i++)
                qbyte[i] = 0;
            for (uint32_t byte = 0; byte <= 0xFF; byte++) {
                memcpy(buf, M, sizeof(M));
                buf[p] = byte;
                memset(&ctx, 0, sizeof(aes256_context));
                aes256_init(&ctx, K);
                aes256_encrypt_ecb(&ctx, buf, r);
                qbyte[buf[q]] += 1;
            }
            int f = 0;
            for (int i = 0; i < 256; i++) {
                if (qbyte[i] != 0)
                    f++;
            }
            printf("%d\t%d\t%d\n", step, r, f);
        }
    }

    close(rand_fd);

    return 0;
}
